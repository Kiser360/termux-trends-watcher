const googleTrends = require('google-trends-api');
const config = require('./config');
const shell = require('execa').shell;

const MIN_MS = 1000 * 60;
const DAY_MS = MIN_MS * 60 * 24;


console.log('Starting watcher');

check();

function recheck() {
    setTimeout(check, MIN_MS * config.queryIntervalMins);
};
function check() {
    googleTrends.interestOverTime({
        keyword: config.keywords,
        startTime: new Date(Date.now() - (DAY_MS * config.daysRange)),
        granularTimeResolution: true,
    })
        .then(res => JSON.parse(res).default.timelineData)
        .then(timeline => {
            let values = timeline.reduce((acc, data) => acc.concat(data.value), []);
            let sumOfValues = values.reduce((acc, val) => acc + val, 0);
            let maxValue = values.reduce((acc, value) => Math.max(acc, value), 0)
            let avgValue = sumOfValues / values.length;
            let spikeValue = maxValue - avgValue;
            console.log("Average Value: " + sumOfValues / values.length);
            console.log("Max Value: " + maxValue);
            let msg = `Your keywords have spiked ${spikeValue}% over the past ${config.daysRange} days`;
            if (spikeValue > config.percentageAboveAverageToTriggerNotification && config.notifications) {
                notify(msg);
            } else {
                console.log(msg);
            }
            recheck();
        });
}

// termux-notification [-c content] [-i id] [-t title] [-u url]
const notify = (msg) => {
    shell(`termux-notification -c "${msg}" -t "Trend Watcher"`, {"shell": "/data/data/com.termux/files/usr/bin/sh"});
}
