# Get notifications for spikes in google trends

## To install
1. Install Termux from the google play store
1. Install Termux:API from the play store
1. Install android api bindings. From termux run 
```
pkg install termux-api
```
1. Install git. From termux run
```
apt install git
```
1. Install Nodejs. From termux run
```
apt install nodejs-current
```
1. Install curl. From termux run
```
apt install curl
```
1. Install Yarn. From termux run
touch ~/.bashrc && curl -o- -L https://yarnpkg.com/install.sh | bash
1. Close Termux to start a new session by reopening it
1. Clone the repo. From termux run
```
git clone https://gitlab.com/Kiser360/termux-trends-watcher.git
```
1. Enter the repo. From termux run
```
cd termux-trends-watcher
```
1. Install Dependencies. From termux run
```
npm run install
```
1. Modify Configuration. From termux modify config.json (may require pkg install vim)
1. Run Watcher. From termux run
```
npm start
```

